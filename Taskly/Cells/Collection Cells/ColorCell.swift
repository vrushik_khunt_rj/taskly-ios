//
//  ColorCell.swift
//  Taskly
//
//  Created by mac on 16/03/22.
//

import UIKit

class ColorCell: UICollectionViewCell {
    @IBOutlet weak var viewColor: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configureCell(itemColor: ColorItem){
        viewColor.backgroundColor = hexStringToUIColor(hex: itemColor.color)
    }
}

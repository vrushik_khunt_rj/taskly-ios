//
//  ManagePermissionCell.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit

class ManagePermissionCell: UICollectionViewCell {
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblPermission: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(permission: PermissionItem){
        lblPermission.text = permission.title
        if permission.isSelected{
            imgCheck.image = UIImage.init(named: "checkbox_sel")
        }else{
            imgCheck.image = UIImage.init(named: "checkbox_")
        }
    }

}

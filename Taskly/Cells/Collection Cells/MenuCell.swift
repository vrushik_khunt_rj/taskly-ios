//
//  MenuCell.swift
//  Taskly
//
//  Created by mac on 09/03/22.
//

import UIKit

class MenuCell: UICollectionViewCell {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblMenuName: UILabel!
    @IBOutlet weak var imgMenu: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(_ menu: MenuItem){
        imgMenu.image = UIImage.init(named: menu.image)
        lblMenuName.text = menu.title
        if menu.isSelected{
            viewBg.backgroundColor = APP_PRIMARY_GREEN_COLOR
            lblMenuName.textColor = .white
            imgMenu.image = imgMenu.image?.withRenderingMode(.alwaysTemplate)
            imgMenu.tintColor = .white
        }else{
            viewBg.backgroundColor = .white
            lblMenuName.textColor = APP_PRIMARY_DEEP_BLUE_COLOR
            imgMenu.image = imgMenu.image?.withRenderingMode(.alwaysTemplate)
            imgMenu.tintColor = APP_PRIMARY_DEEP_BLUE_COLOR
        }
    }

}

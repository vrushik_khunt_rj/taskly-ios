//
//  ModuleCell.swift
//  Taskly
//
//  Created by mac on 25/03/22.
//

import UIKit

class ModuleCell: UICollectionViewCell {
    @IBOutlet weak var btnOnoff: UIButton!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnOnoff.setImage(UIImage.init(named: "switch_on"), for: .selected)
        btnOnoff.setImage(UIImage.init(named: "switch_off"), for: .normal)
    }
    func configureCell(_ module: ModuleItem){
        btnOnoff.isSelected = module.isSelected == true ? true : false
        lblName.text = module.title
        
    }
    @IBAction func onClickSwitch(_ sender: Any){
        btnOnoff.isSelected = btnOnoff.isSelected == true ? false : true
    }
}

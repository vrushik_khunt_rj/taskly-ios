//
//  TimeSheetListCell.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit

class TimeSheetListCell: UICollectionViewCell {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewBox: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(timeItem: TimeSheetListItem){
        lblTime.text = timeItem.time
        lblDay.text = timeItem.day
        lblDate.text = timeItem.date
        viewBox.layer.borderColor = timeItem.time == "" ? APP_SHADOWVIEW_BORDER_COLOR.cgColor : APP_PRIMARY_GREEN_COLOR.cgColor
    }

}

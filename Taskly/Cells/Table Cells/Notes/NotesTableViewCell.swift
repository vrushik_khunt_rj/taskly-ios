//
//  NotesTableViewCell.swift
//  Taskly
//
//  Created by mac on 14/03/22.
//

import UIKit

class NotesTableViewCell: UITableViewCell {
    @IBOutlet weak var viewNewNote: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        viewNewNote.addLineDashedStroke(pattern: [4,2], radius: 20, color: hexStringToUIColor(hex: "717887").cgColor)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

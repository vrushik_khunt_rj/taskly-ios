//
//  UsersTableViewCell.swift
//  Taskly
//
//  Created by mac on 14/03/22.
//

import UIKit
import MultiProgressView

class UsersTableViewCell: UITableViewCell {
    @IBOutlet weak var progressView: MultiProgressView!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var viewProjectTasks: UIView!
    @IBOutlet var heightProjectTasks: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.height / 2
        progressView.dataSource = self
        progressView.delegate = self
        progressView.lineCap = .round
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0,
                       options: .curveLinear,
                       animations: {
                        self.progressView.setProgress(section: 0, to: 0.4)
                        self.progressView.setProgress(section: 1, to: 0.25)
                        self.progressView.setProgress(section: 2, to: 0.1)
        })
        // Initialization code
    }
    func configureCellWith(dictionary:[String:Any],isforProjects:Bool = false){
        if isforProjects{
            self.viewProjectTasks.isHidden = false
            self.heightProjectTasks.constant = 50
        }else{
            self.viewProjectTasks.isHidden = true
            self.heightProjectTasks.constant = 0

        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension UsersTableViewCell: MultiProgressViewDelegate, MultiProgressViewDataSource{
    
    
    func numberOfSections(in progressView: MultiProgressView) -> Int{
        return 3
    }
    func progressView(_ progressView: MultiProgressView, viewForSection section: Int) -> ProgressViewSection{
        let progressSection = ProgressViewSection()
        if section == 0{
            progressSection.backgroundColor = hexStringToUIColor(hex: "6FD943")
        }else if section == 1{
            progressSection.backgroundColor = hexStringToUIColor(hex: "153364")
        }else if section == 2{
            progressSection.backgroundColor = hexStringToUIColor(hex: "F0F1F5")
        }
        return progressSection
    }

}

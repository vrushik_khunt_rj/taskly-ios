//
//  ZoomMeetingCell.swift
//  Taskly
//
//  Created by mac on 25/03/22.
//

import UIKit

class ZoomMeetingCell: UITableViewCell {
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblProjectTitle: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var lblDeveloerType: UILabel!
    @IBOutlet weak var lblSubtask: UILabel!
    @IBOutlet weak var viewMore: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

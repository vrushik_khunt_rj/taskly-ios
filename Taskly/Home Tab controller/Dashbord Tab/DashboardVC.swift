//
//  DashboardVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit
import JJFloatingActionButton

class DashboardVC: UIViewController, JJFloatingActionButtonDelegate,FSCalendarDataSource, FSCalendarDelegate {
    
    func floatingActionButtonWillOpen(_ button: JJFloatingActionButton) {
        actionButton.buttonColor = .white
        viewCircle.isHidden = false
    }
    func floatingActionButtonWillClose(_ button: JJFloatingActionButton) {
        actionButton.buttonColor = hexStringToUIColor(hex: "6FD943")
        viewCircle.isHidden = true
    }
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(self.dateFormatter.string(from: date))")
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        print("selected dates is \(selectedDates)")
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
    }

    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }

    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var tableTodaysFocus: UITableView!
    @IBOutlet weak var tableSummary: UITableView!
    @IBOutlet weak var segmentTodays: HBSegmentedControl!
    @IBOutlet weak var segmentTask: HBSegmentedControl!
    @IBOutlet weak var viewCircle: UIView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var lblNotificationCount: UILabel!


    fileprivate let actionButton = JJFloatingActionButton()
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.height/2
        actionButton.buttonColor = hexStringToUIColor(hex: "6FD943")
        actionButton.delegate = self
        actionButton.buttonImageColor = hexStringToUIColor(hex: "172C4E")
        actionButton.overlayView.backgroundColor = .clear
        actionButton.buttonImageSize = CGSize(width: 16, height: 16)
        actionButton.itemAnimationConfiguration = .circularSlideIn(withRadius: 130)
        actionButton.buttonAnimationConfiguration = .rotation(toAngle: .pi * 3 / 4)
        actionButton.buttonAnimationConfiguration.opening.duration = 0.8
        actionButton.buttonAnimationConfiguration.closing.duration = 0.6
        for item in actionButton.items{
            item.imageView.tintColor = hexStringToUIColor(hex: "172C4E")
        }
        if #available(iOS 13.0, *) {
            actionButton.buttonImage = UIImage.init(named: "ic_plus")?.withTintColor(hexStringToUIColor(hex: "172C4E"))
        } else {
            // Fallback on earlier versions
        }
        let imageProj = UIImage.init(named: "ic_project")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imgageTimesht = UIImage.init(named: "ic_timesheet")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imageInvo = UIImage.init(named: "tab_invoices")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imageTracker = UIImage.init(named: "tab_tracker")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imageTasks = UIImage.init(named: "tab_tasks")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))

        actionButton.addItem(image: imageProj) { item in
//            Helper.showAlert(for: item)
            let vc = AddNewProjectVC(nibName: "AddNewProjectVC", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext

            self.navigationController?.present(vc, animated: true, completion: nil)

        }
        actionButton.addItem(image: imgageTimesht) { item in
            //            Helper.showAlert(for: item)
//            let vc = InviteNewClientVC(nibName: "InviteNewClientVC", bundle: nil)
//            vc.view.backgroundColor = .clear
//            vc.modalPresentationStyle = .overCurrentContext
//
//            self.navigationController?.present(vc, animated: true, completion: nil)

        }

        actionButton.addItem(image: imageInvo) { item in
//            Helper.showAlert(for: item)
            let vc = AddNewInvoiceVC(nibName: "AddNewInvoiceVC", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext

            self.navigationController?.present(vc, animated: true, completion: nil)

        }
        actionButton.addItem(image: imageTracker) { item in
            //            Helper.showAlert(for: item)
        }


        actionButton.addItem(image: imageTasks) { item in
            let vc = AddNewTaskVC(nibName: "AddNewTaskVC", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext

            self.navigationController?.present(vc, animated: true, completion: nil)
//            Helper.showAlert(for: item)
        }
        
//        actionButton.display(inViewController: self)
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -18).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -36).isActive = true
        viewCircle.center = actionButton.center
        viewCircle.translatesAutoresizingMaskIntoConstraints = false

        
        tableTodaysFocus.register(UINib.init(nibName: "TasksKBTableViewCell", bundle: nil), forCellReuseIdentifier: "TasksKBTableViewCell")
        tableTodaysFocus.tableFooterView = UIView()
        tableSummary.register(UINib.init(nibName: "TaskSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "TaskSummaryTableViewCell")
        tableSummary.tableFooterView = UIView()

        //money
        segmentTodays.items = ["Today", "Week", "Month"]
        segmentTodays.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentTodays.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentTodays.selectedLabelColor = .white
        segmentTodays.backgroundColor = .clear
        segmentTodays.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentTodays.selectedIndex = 0

        //invoice
        segmentTask.items = ["Today", "Week", "Month"]
        segmentTask.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentTask.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentTask.selectedLabelColor = .white
        segmentTask.backgroundColor = .clear
        segmentTask.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentTask.selectedIndex = 0

        // Do any additional setup after loading the view.
        
        self.calendar.select(Date())
        self.calendar.scope = .month

    }
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Dashboard"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext

        self.navigationController?.present(vc, animated: false)
    }
    
    @IBAction func onClickNotifications(_ sender: Any){
        let vc = NotificationsViewController(nibName: "NotificationsViewController", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext

        self.navigationController?.present(vc, animated: true)

    }
    


}

extension DashboardVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableTodaysFocus{
            return 4
        }
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableTodaysFocus{
            let cell: TasksKBTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TasksKBTableViewCell") as! TasksKBTableViewCell
            cell.selectionStyle = .none
            return cell
        }
        let cell: TaskSummaryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TaskSummaryTableViewCell") as! TaskSummaryTableViewCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tableTodaysFocus{
            return 240
        }
        return 110
    }
}

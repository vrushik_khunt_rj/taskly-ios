//
//  InvoicesDetailViewController.swift
//  Taskly
//
//  Created by mac on 11/03/22.
//

import UIKit

class InvoicesDetailViewController: UIViewController {
    @IBOutlet weak var tableOrder: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableOrder.register(UINib.init(nibName: "OrderSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderSummaryTableViewCell")
        tableOrder.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }


}

extension InvoicesDetailViewController :  UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderSummaryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OrderSummaryTableViewCell") as! OrderSummaryTableViewCell
        cell.selectionStyle = .none
        return cell
    }
}

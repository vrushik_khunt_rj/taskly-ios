//
//  InvoicesVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit

class InvoicesVC: UIViewController {
    @IBOutlet weak var tableInvoices: UITableView!
    @IBOutlet weak var tableQuickInvoice: UITableView!
    @IBOutlet weak var segmentMoney: HBSegmentedControl!
    @IBOutlet weak var segmentInvoice: HBSegmentedControl!
    @IBOutlet weak var segmentQuick: HBSegmentedControl!
    @IBOutlet var chart: OMScrollableChart!

    let chartPoints: [Float] =   [1510, 100,
                                  3000, 100,
                                  1200, 13000,
                                 15000, -1500,
                                 800, 1000,
                                 6000, 1300]
    var pathsToAnimate = [[UIBezierPath]]()
    var animationTimingTable: [AnimationTiming] = [
        .none,
        .none,
        .oneShot,
        .none,
        .none,
        .none,
        .none
    ]
    var opacityTableLine: [CGFloat] = [1, 1, 1, 1, 1, 0, 0]
    var opacityTableBar: [CGFloat]  = [0, 0, 0, 0, 0, 1, 1]
    var curOpacityTable: [CGFloat]  = []

    var renderType: OMScrollableChart.RenderType = .discrete


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //money
        segmentMoney.items = ["Today", "Week", "Month"]
        segmentMoney.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentMoney.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentMoney.selectedLabelColor = .white
        segmentMoney.backgroundColor = .clear
        segmentMoney.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentMoney.selectedIndex = 0

        //invoice
        segmentInvoice.items = ["Today", "Week", "Month"]
        segmentInvoice.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentInvoice.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentInvoice.selectedLabelColor = .white
        segmentInvoice.backgroundColor = .clear
        segmentInvoice.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentInvoice.selectedIndex = 0
        
        //quick
        segmentQuick.items = ["Featured", "Sort by A-Z"]
        segmentQuick.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentQuick.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentQuick.selectedLabelColor = .white
        segmentQuick.backgroundColor = .clear
        segmentQuick.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentQuick.selectedIndex = 0
        
        //chart
        chart.bounces = false
        chart.dataSource = self
        chart.renderSource = self
        chart.renderDelegate  = self
        chart.backgroundColor = .clear
        chart.isPagingEnabled = true
        curOpacityTable = opacityTableLine
        chart.ruleFont = UIFont.init(name: FONT_Outfit_Regular, size: 10)!
        chart.polylineInterpolation = .smoothed

//        _ = chart.updateDataSourceData()


        tableInvoices.register(UINib.init(nibName: "InvoiceTableViewCell", bundle: nil), forCellReuseIdentifier: "InvoiceTableViewCell")
        tableQuickInvoice.register(UINib.init(nibName: "QuickInvoiceTableViewCell", bundle: nil), forCellReuseIdentifier: "QuickInvoiceTableViewCell")
        tableInvoices.tableFooterView = UIView()
        tableQuickInvoice.tableFooterView = UIView()
//        tableInvoices.reloadData()
        tableQuickInvoice.reloadData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Invoices"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
    }
    @IBAction func onClickNewInvoice(_ sender: Any){
        let vc = AddNewInvoiceVC(nibName: "AddNewInvoiceVC", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
    }

}

extension InvoicesVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableInvoices{
            return 3
        }
        return 3
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableInvoices{
            let cell: InvoiceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InvoiceTableViewCell") as! InvoiceTableViewCell
            cell.selectionStyle = .none
            cell.currentIndex = indexPath.row
            cell.configureCell(["String" : "Any"])
            return cell
        }
        let cell: QuickInvoiceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "QuickInvoiceTableViewCell") as! QuickInvoiceTableViewCell
        cell.selectionStyle = .none
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableInvoices{
            let vc = InvoicesDetailViewController(nibName: "InvoicesDetailViewController", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: true)

        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tableInvoices{
            return 130
        }
        return 110

    }
    
}
extension InvoicesVC: OMScrollableChartDataSource, OMScrollableChartRenderableProtocol, OMScrollableChartRenderableDelegateProtocol {
    
    func queryAnimation(chart: OMScrollableChart, renderIndex: Int) -> AnimationTiming {
        return animationTimingTable[renderIndex]
    }
    func didSelectDataIndex(chart: OMScrollableChart, renderIndex: Int, dataIndex: Int, layer: CALayer) {
        switch renderIndex {
        case 0:break
        case 1: chart.renderSelectedPointsLayer?.position =  layer.position
        case 2:break
        case 3:break
        case 4:break
        default: break
        }
    }
    func animationDidEnded(chart: OMScrollableChart, renderIndex: Int, animation: CAAnimation) {
        switch renderIndex {
        case 0: break
        case 1: break
        case 2: animationTimingTable[renderIndex] = .none
        case 3: break
        case 4: break
        default: break
        }
    }
    func animateLayers(chart: OMScrollableChart,
                       renderIndex: Int,
                       layerIndex: Int,
                       layer: OMGradientShapeClipLayer) -> CAAnimation? {
        switch OMScrollableChart.Renders(rawValue: renderIndex) {
        case .points, .selectedPoint, .currentPoint, .segments:
            return nil
        case .polyline:
            guard let polylinePath = chart.polylinePath else {
                return nil
            }
            return chart.animateLayerPathRideToPoint( polylinePath,
                                                      layerToRide: layer,
                                                      pointIndex: chart.numberOfSections,
                                                      duration: 10)
            
        case .bar1:
            let pathStart = pathsToAnimate[renderIndex - 3][layerIndex]
            return chart.animateLayerPath( layer,
                                           pathStart: pathStart,
                                           pathEnd: UIBezierPath( cgPath: layer.path!))
        case .bar2:
            let pathStart = pathsToAnimate[renderIndex - 3][layerIndex]
            return chart.animateLayerPath( layer,
                                           pathStart: pathStart,
                                           pathEnd: UIBezierPath( cgPath: layer.path!) )
            
        default:
            return nil
        }
    }
    var numberOfRenders: Int {
        return OMScrollableChart.Renders.base.rawValue
    }
    func dataPoints(chart: OMScrollableChart, renderIndex: Int, section: Int) -> [Float] {
        return chartPoints
    }
    func dataLayers(chart: OMScrollableChart, renderIndex: Int, section: Int, points: [CGPoint]) -> [OMGradientShapeClipLayer] {
        switch OMScrollableChart.Renders(rawValue:  renderIndex) {
//        case 0:
//            let layers = chart.updatePolylineLayer(lineWidth: 4,
//                                                   color: .greyishBlue)
//            layers.forEach({$0.name = "polyline"}) //debug
//            return layers
//        case 1:
//            let layers = chart.createPointsLayers(points,
//                                                  size: CGSize(width: 8, height: 8),
//                                                  color: .greyishBlue)
//            layers.forEach({$0.name = "point"})  //debug
//            return layers
//        case 2:
////            if let point = chart.maxPoint(renderIndex: renderIndex) {
////                let layer = chart.createPointLayer(point,
////                                                   size: CGSize(width: 12, height: 12),
////                                                   color: .darkGreyBlueTwo)
////                layer.name = "selectedPoint"  //debug
////                return [layer]
////            }
////            return []
//            return []
        case .bar1:
            let layers =  chart.createRectangleLayers(points, columnIndex: 1, count: 6,
                                                      color: .black)
            layers.forEach({$0.name = "bar income"})  //debug
            self.pathsToAnimate.insert(
                chart.createInverseRectanglePaths(points, columnIndex: 1, count: 6),
                at: 0)
            return layers
        case .bar2:
            
            let layers =  chart.createRectangleLayers(points, columnIndex: 4, count: 6,
                                                      color: .green)
            layers.forEach({$0.name = "bar outcome"})  //debug
            self.pathsToAnimate.insert(
                chart.createInverseRectanglePaths(points, columnIndex: 4, count: 6),
                at: 1)
            return layers
            
        default:
            return []
        }
    }
    func footerSectionsText(chart: OMScrollableChart) -> [String]? {
        return nil
    }
    func dataPointTootipText(chart: OMScrollableChart, renderIndex: Int, dataIndex: Int, section: Int) -> String? {
        return nil
    }
    func dataOfRender(chart: OMScrollableChart, renderIndex: Int) -> OMScrollableChart.RenderType {
        return renderType
    }
    func dataSectionForIndex(chart: OMScrollableChart, dataIndex: Int, section: Int) -> String? {
        return nil
    }
    func layerOpacity(chart: OMScrollableChart, renderIndex: Int) -> CGFloat {
        return curOpacityTable[renderIndex]
    }
    func numberOfPages(chart: OMScrollableChart) -> CGFloat {
        return 2
    }
    func numberOfSectionsPerPage(chart: OMScrollableChart) -> Int {
        return 6
    }

}

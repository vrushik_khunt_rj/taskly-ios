//
//  InvoiceTemplateViewController.swift
//  Taskly
//
//  Created by mac on 16/03/22.
//

import UIKit

class InvoiceTemplateViewController: UIViewController {
    @IBOutlet weak var ColorCollection: UICollectionView!
    @IBOutlet weak var tableOrder: UITableView!
    @IBOutlet weak var viewTheme: UIView!

    var colors = [ColorItem(colour: "FFC700"),
                  ColorItem(colour: "FF754C"),
                  ColorItem(colour: "172C4E"),
                  ColorItem(colour: "89E065"),
                  ColorItem(colour: "5A6579"),
                  ColorItem(colour: "172C4E"),
                  ColorItem(colour: "FFC700"),
                  ColorItem(colour: "FF754C"),
                  ColorItem(colour: "172C4E"),
                  ColorItem(colour: "89E065")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableOrder.register(UINib.init(nibName: "OrderSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderSummaryTableViewCell")
        tableOrder.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }


}
extension InvoiceTemplateViewController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath) as! ColorCell
        cell.configureCell(itemColor: colors[indexPath.item])
        return cell
    }
}
extension InvoiceTemplateViewController :  UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderSummaryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OrderSummaryTableViewCell") as! OrderSummaryTableViewCell
        cell.selectionStyle = .none
        return cell
    }
}

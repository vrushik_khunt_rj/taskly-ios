//
//  PaymentsViewController.swift
//  Taskly
//
//  Created by mac on 16/03/22.
//

import UIKit

class PaymentsViewController: UIViewController {

    @IBOutlet weak var viewPaymentSubinfo: UIView!
    @IBOutlet var heightPaymentSubinfo: NSLayoutConstraint!
    @IBOutlet weak var btnSwitch: UIButton!
    @IBOutlet weak var lblStatusofButton: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        btnSwitch.setImage(UIImage.init(named: "switch_on"), for: .selected)
        btnSwitch.setImage(UIImage.init(named: "switch_off"), for: .normal)

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickPaymentSwitch(_ sender: UIButton){
        if !btnSwitch.isSelected{
            viewPaymentSubinfo.isHidden = false
            heightPaymentSubinfo.constant = 160
            lblStatusofButton.text = "Disable"
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            btnSwitch.isSelected = true
        }else{
            viewPaymentSubinfo.isHidden = true
            heightPaymentSubinfo.constant = 0
            lblStatusofButton.text = "Enable"

            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            btnSwitch.isSelected = false

        }
    }
    

}

//
//  SettingsVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit

class SettingsVC: UIViewController {
    @IBOutlet weak var tableSettings: UITableView!
    
    var arrSettings = [SettingItem(title: "Site Settings", image: "set_site"),
                       SettingItem(title: "Task Stages", image: "set_task"),
                       SettingItem(title: "Bug Stages", image: "set_bug"),
                       SettingItem(title: "Taxes", image: "set_tax"),
                       SettingItem(title: "Billing", image: "set_bill"),
                       SettingItem(title: "Payment", image: "set_payment"),
                       SettingItem(title: "Invoices", image: "set_invoice"),
                       SettingItem(title: "Zoom Settings", image: "set_zoom"),
                       SettingItem(title: "Slack Settings", image: "set_slack"),
                       SettingItem(title: "Telegram Settings", image: "set_telegram")]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableSettings.register(UINib.init(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
        tableSettings.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    func openViewControllerWithName(name: String){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: name)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any){
//        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
//        vc.parentVC = self
//        vc.selectedItem = "Settings"
//        vc.view.backgroundColor = .clear
//        vc.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(vc, animated: true)
        self.navigationController?.popViewController(animated: true)
    }

}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        cell.selectionStyle = .none
        cell.configureCellWith(arrSettings[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = arrSettings[indexPath.row].title
        var vcName : String = ""
        if title == "Site Settings"{
            vcName = "SiteSettingsViewController"
        }else if title == "Task Stages"{
            vcName = "TaskStagesViewController"
        }else if title == "Bug Stages"{
            vcName = "TaskStagesViewController"
        }else if title == "Taxes"{
            vcName = "TaxPlansViewController"
        }else if title == "Billing"{
//            vcName = "ChatViewController"
        }else if title == "Payment"{
            vcName = "PaymentsViewController"
        }else if title == "Invoices"{
            vcName = "InvoiceTemplateViewController"
        }else if title == "Zoom Settings"{
            vcName = "ZoomDetailsVC"
        }else if title == "Slack Settings"{
            vcName = "SlackSettingsVC"
        }else if title == "Telegram Settings"{
            vcName = "TelegramSettingsVC"
        }
        
        if vcName != ""{
            self.openViewControllerWithName(name: vcName)
        }

    }
}

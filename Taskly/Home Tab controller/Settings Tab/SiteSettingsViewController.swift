//
//  SiteSettingsViewController.swift
//  Taskly
//
//  Created by mac on 15/03/22.
//

import UIKit

class SiteSettingsViewController: UIViewController {
    @IBOutlet weak var viewAddAttachment: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewAddAttachment.addLineDashedStroke(pattern: [4,4], radius: 20, color: hexStringToUIColor(hex: "D1D1D1").cgColor)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }

}

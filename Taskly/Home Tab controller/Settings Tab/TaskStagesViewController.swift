//
//  TaskStagesViewController.swift
//  Taskly
//
//  Created by mac on 15/03/22.
//

import UIKit

class TaskStagesViewController: UIViewController {
    @IBOutlet weak var tableTaskStage: UITableView!
    @IBOutlet weak var tableBugStage: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableTaskStage.register(UINib.init(nibName: "TasksStagesCell", bundle: nil), forCellReuseIdentifier: "TasksStagesCell")
        tableBugStage.register(UINib.init(nibName: "TasksStagesCell", bundle: nil), forCellReuseIdentifier: "TasksStagesCell")
        tableTaskStage.tableFooterView = UIView()
        tableBugStage.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickCreateState(_ sender: Any){
        let vc = AddStagesVC(nibName: "AddStagesVC", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }


}

extension TaskStagesViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableTaskStage{
            return 3
        }
        return 3

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableTaskStage{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TasksStagesCell") as! TasksStagesCell
            cell.selectionStyle = .none
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "TasksStagesCell") as! TasksStagesCell
        cell.selectionStyle = .none
        return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}

//
//  AddNewTaskVC.swift
//  Taskly
//
//  Created by mac on 24/03/22.
//

import UIKit

class AddNewTaskVC: UIViewController {
    @IBOutlet weak var tableUsers: UITableView!
    @IBOutlet weak var viewAddNewUser: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewAddNewUser.addLineDashedStroke(pattern: [4,2], radius: 20, color: hexStringToUIColor(hex: "717887").cgColor)
        tableUsers.register(UINib.init(nibName: "UserListsTVCell", bundle: nil), forCellReuseIdentifier: "UserListsTVCell")
        tableUsers.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickAddNewUser(_ sender: Any){
        let vc = InviteNewUserVC(nibName: "InviteNewUserVC", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true)

    }
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddNewTaskVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListsTVCell") as! UserListsTVCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        45
    }
}

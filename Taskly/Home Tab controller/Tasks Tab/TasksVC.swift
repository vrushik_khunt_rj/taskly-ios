//
//  TasksVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit
import MobileCoreServices


class TasksVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionKanban: UICollectionView!
    @IBOutlet weak var tableSummary: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var segmentControl: HBSegmentedControl!
    
    var boards = [
        Board(title: "Backlog", items: ["Database Migration"]),
        Board(title: "In Progress", items: ["Push Notification", "Analytics", "Machine Learning"]),
        Board(title: "Testing", items: ["Push Notification", "Analytics", "Machine Learning"]),
        Board(title: "Done", items: ["System Architecture", "Alert & Debugging"])
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        segmentControl.items = ["Today", "Week", "Month"]
        segmentControl.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentControl.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentControl.selectedLabelColor = .white
        segmentControl.backgroundColor = .clear
        segmentControl.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentControl.selectedIndex = 0
//        segmentControl.addTarget(self, action: #selector(segmentValueChanged(_:)), for: .valueChanged)
        tableSummary.register(UINib.init(nibName: "TaskSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "TaskSummaryTableViewCell")
        tableSummary.tableFooterView = UIView()
        setupNavigationBar()
        updateCollectionViewItem(with: view.bounds.size)
    }
    
    private func setupNavigationBar() {
        setupAddButtonItem()
    }
    
    @IBAction func onClickMenuBtn(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Tasks"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateCollectionViewItem(with: size)
    }
    
    private func updateCollectionViewItem(with size: CGSize) {
        guard let layout = collectionKanban.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        layout.itemSize = CGSize(width: 225, height: size.height * 0.8)
    }
    
    func setupAddButtonItem() {
        let addButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addListTapped(_:)))
        navigationItem.rightBarButtonItem = addButtonItem
    }
    
    func setupRemoveBarButtonItem() {
        let button = UIButton(type: .system)
        button.setTitle("Delete", for: .normal)
        button.setTitleColor(.red, for: .normal)
        button.addInteraction(UIDropInteraction(delegate: self))
        let removeBarButtonItem = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = removeBarButtonItem
    }

    @objc func addListTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Add List", message: nil, preferredStyle: .alert)
        alertController.addTextField(configurationHandler: nil)
        alertController.addAction(UIAlertAction(title: "Add", style: .default, handler: { (_) in
            guard let text = alertController.textFields?.first?.text, !text.isEmpty else {
                return
            }
            
            self.boards.append(Board(title: text, items: []))
            
            let addedIndexPath = IndexPath(item: self.boards.count - 1, section: 0)
            
            self.collectionKanban.insertItems(at: [addedIndexPath])
            self.collectionKanban.scrollToItem(at: addedIndexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true)
    }
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return boards.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 302, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! TaskKBContainerCollectionViewCell
        cell.lblHeader.text = boards[indexPath.item].title
        cell.setup(with: boards[indexPath.item])
        cell.parentVC = self
        return cell
    }
    
}

extension TasksVC: UIDropInteractionDelegate {
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .move)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        
        if session.hasItemsConforming(toTypeIdentifiers: [kUTTypePlainText as String]) {
            session.loadObjects(ofClass: NSString.self) { (items) in
                guard let _ = items.first as? String else {
                    return
                }
                
                if let (dataSource, sourceIndexPath, tableView) = session.localDragSession?.localContext as? (Board, IndexPath, UITableView) {
                    tableView.beginUpdates()
                    dataSource.items.remove(at: sourceIndexPath.row)
                    tableView.deleteRows(at: [sourceIndexPath], with: .automatic)
                    tableView.endUpdates()
                }
            }
        }
    }
}


extension TasksVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        90
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TaskSummaryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TaskSummaryTableViewCell") as! TaskSummaryTableViewCell
        cell.selectionStyle = .none
        return cell
    }
}

//
//  TrackerViewController.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit

class TrackerViewController: UIViewController {
    @IBOutlet weak var tableStartedTracker: UITableView!
    @IBOutlet weak var tableTrackersList: UITableView!
    @IBOutlet weak var subtasksCV:UICollectionView!
    @IBOutlet weak var segmentTrackerList: HBSegmentedControl!
    
    private var selected = [String]()
    private var titles = [String]()


    override func viewDidLoad() {
        super.viewDidLoad()
        titles = ["Define users and Workflow", "Workflow settings", "Workflow settings", "+ More"]
        subtasksCV.register(UINib.init(nibName: "SubtaskCell", bundle: nil), forCellWithReuseIdentifier: "SubtaskCell")
        let layout = TagFlowLayout()
        layout.estimatedItemSize = CGSize(width: 140, height: 24)
        subtasksCV.collectionViewLayout = layout

        tableStartedTracker.register(UINib.init(nibName: "TrackersCell", bundle: nil), forCellReuseIdentifier: "TrackersCell")
        tableTrackersList.register(UINib.init(nibName: "TrackersCell", bundle: nil), forCellReuseIdentifier: "TrackersCell")
        tableStartedTracker.tableFooterView = UIView()
        tableTrackersList.tableFooterView = UIView()
        
        segmentTrackerList.items = ["Today", "Week", "Month"]
        segmentTrackerList.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentTrackerList.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentTrackerList.selectedLabelColor = .white
        segmentTrackerList.backgroundColor = .clear
        segmentTrackerList.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentTrackerList.selectedIndex = 0

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
    }

    
}

extension TrackerViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableStartedTracker{
            return 1
        }
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackersCell") as! TrackersCell
        cell.selectionStyle = .none
        
        cell.configureCellWith(dict: [:], isTrackerStarted: tableView == self.tableStartedTracker ? true : false)

        return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
}
extension TrackerViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubtaskCell", for: indexPath) as! SubtaskCell
        cell.configureCell(titles[indexPath.row], isSelected: selected.contains(titles[indexPath.row]) ? true : false)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? SubtaskCell, let text = cell.lblSubtask.text else {return}
        
        if selected.contains(text) {
            selected = selected.filter{$0 != text}
        } else {
            selected.append(text)
        }
        collectionView.reloadData()

    }
}

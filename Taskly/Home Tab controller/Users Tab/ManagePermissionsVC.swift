//
//  ManagePermissionsVC.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit

class ManagePermissionsVC: UIViewController {
    @IBOutlet weak var permissionsCV: UICollectionView!
    var arrPermissions = [PermissionItem(name: "Dashboard", selected: false),
                          PermissionItem(name: "Show Dashboard", selected: true),
                          PermissionItem(name: "Manage Account", selected: true),
                          PermissionItem(name: "Edit Account", selected: true),
                          PermissionItem(name: "Change Password", selected: false),
                          PermissionItem(name: "Expense", selected: true),
                          PermissionItem(name: "Create expense", selected: true),
                          PermissionItem(name: "Edit expense", selected: true),
                          PermissionItem(name: "Delete expense", selected: false),
                          PermissionItem(name: "Manage invoice", selected: false),
                          PermissionItem(name: "Dashboard", selected: false),
                          PermissionItem(name: "Show Dashboard", selected: true),
                          PermissionItem(name: "Manage Account", selected: true),
                          PermissionItem(name: "Edit Account", selected: true),
                          PermissionItem(name: "Change Password", selected: false),
                          PermissionItem(name: "Expense", selected: true),
                          PermissionItem(name: "Create expense", selected: true),
                          PermissionItem(name: "Edit expense", selected: true),
                          PermissionItem(name: "Delete expense", selected: false),
                          PermissionItem(name: "Manage invoice", selected: false)]

    override func viewDidLoad() {
        super.viewDidLoad()
        permissionsCV.register(UINib.init(nibName: "ManagePermissionCell", bundle: nil), forCellWithReuseIdentifier: "ManagePermissionCell")
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }


}

extension ManagePermissionsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPermissions.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ManagePermissionCell", for: indexPath) as! ManagePermissionCell
        cell.configureCell(permission: arrPermissions[indexPath.item])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let permission = arrPermissions[indexPath.item]
        permission.isSelected = permission.isSelected == true ? false : true
        self.permissionsCV.reloadData()
    }
}

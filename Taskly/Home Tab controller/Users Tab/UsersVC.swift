//
//  UsersVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit

class UsersVC: UIViewController {

    @IBOutlet weak var tableUsers: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableUsers.register(UINib.init(nibName: "UsersTableViewCell", bundle: nil), forCellReuseIdentifier: "UsersTableViewCell")
        tableUsers.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Users"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)

    }
    @IBAction func onClickNewUser(_ sender: Any){
        let vc = InviteNewUserVC(nibName: "InviteNewUserVC", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)

    }

}

extension UsersVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UsersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UsersTableViewCell") as! UsersTableViewCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // editAction code
        }
        let changePermission = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // changePermission code
            let vc = ManagePermissionsVC(nibName: "ManagePermissionsVC", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext

            self?.navigationController?.present(vc, animated: true, completion: nil)
        }
        // Set the button's images
        editAction.image = UIImage.init(named: "ic_edit")
        editAction.title = "Edit"
        changePermission.image = UIImage.init(named: "ic_permission")
        changePermission.title = "Change Permission"
        editAction.backgroundColor = hexStringToUIColor(hex: "FEC601")
        changePermission.backgroundColor = hexStringToUIColor(hex: "153364")

        return UISwipeActionsConfiguration.init(actions: [editAction,changePermission])

    }
}

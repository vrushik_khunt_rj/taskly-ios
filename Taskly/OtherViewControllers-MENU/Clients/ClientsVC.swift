//
//  ClientsVC.swift
//  Taskly
//
//  Created by mac on 25/03/22.
//

import UIKit

class ClientsVC: UIViewController {
    @IBOutlet weak var tableClients: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableClients.register(UINib.init(nibName: "ClientsListCell", bundle: nil), forCellReuseIdentifier: "ClientsListCell")
        tableClients.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }

    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickAddNewClient(_ sender: Any){
        let vc = InviteNewClientVC(nibName: "InviteNewClientVC", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ClientsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientsListCell") as! ClientsListCell
        cell.selectionStyle =  .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let passwordAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // passwordAction code
            let vc = SetClientPasswordVC(nibName: "SetClientPasswordVC", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self?.navigationController?.present(vc, animated: true, completion: nil)
        }

        let editAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // editAction code
        }
        let deleteAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // deleteAction code
           
        }
        // Set the button's images
        passwordAction.image = UIImage.init(named: "ic_lock")
        passwordAction.title = "Password"
        editAction.image = UIImage.init(named: "ic_edit")
        editAction.title = "Edit"
        deleteAction.image = UIImage.init(named: "ic_delete")
        deleteAction.title = "Delete"
        passwordAction.backgroundColor = hexStringToUIColor(hex: "717887")
        editAction.backgroundColor = hexStringToUIColor(hex: "FEC601")
        deleteAction.backgroundColor = hexStringToUIColor(hex: "FF754C")

        return UISwipeActionsConfiguration.init(actions: [deleteAction,editAction,passwordAction])

    }
    
}

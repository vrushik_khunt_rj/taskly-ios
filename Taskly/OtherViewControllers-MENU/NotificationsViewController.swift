//
//  NotificationsViewController.swift
//  Taskly
//
//  Created by mac on 16/03/22.
//

import UIKit

class NotificationsViewController: UIViewController {
    @IBOutlet weak var tableNotifications: UITableView!

    @IBOutlet weak var segmentNoti: HBSegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableNotifications.register(UINib.init(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        tableNotifications.tableFooterView = UIView()
        
        segmentNoti.items = ["Today", "Week", "Month"]
        segmentNoti.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentNoti.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentNoti.selectedLabelColor = .white
        segmentNoti.backgroundColor = .clear
        segmentNoti.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentNoti.selectedIndex = 0

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }



}
extension NotificationsViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        cell.selectionStyle = .none
        cell.configureCell()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

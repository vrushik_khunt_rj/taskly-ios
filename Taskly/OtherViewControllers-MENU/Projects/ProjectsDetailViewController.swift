//
//  ProjectsDetailViewController.swift
//  Taskly
//
//  Created by mac on 15/03/22.
//

import UIKit

class ProjectsDetailViewController: UIViewController {
    @IBOutlet weak var viewAddAttachment: UIView!
    @IBOutlet weak var imgProfile: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
        viewAddAttachment.addLineDashedStroke(pattern: [4,2], radius: 20, color: hexStringToUIColor(hex: "717887").cgColor)

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }

}

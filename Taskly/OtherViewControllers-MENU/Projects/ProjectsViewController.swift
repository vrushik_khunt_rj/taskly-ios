//
//  ProjectsViewController.swift
//  Taskly
//
//  Created by mac on 15/03/22.
//

import UIKit

class ProjectsViewController: UIViewController {
    @IBOutlet weak var tableProjects: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableProjects.register(UINib.init(nibName: "UsersTableViewCell", bundle: nil), forCellReuseIdentifier: "UsersTableViewCell")
        tableProjects.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickMenu(_ sender: Any){
//        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
//        vc.parentVC = self
//        vc.selectedItem = "Projects"
//        vc.view.backgroundColor = .clear
//        vc.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(vc, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickNewProject(_ sender: Any){
        let vc = AddNewProjectVC(nibName: "AddNewProjectVC", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }


}

extension ProjectsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersTableViewCell") as! UsersTableViewCell
        cell.selectionStyle = .none
        cell.configureCellWith(dictionary: [:], isforProjects: true)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ProjectsDetailViewController(nibName: "ProjectsDetailViewController", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)

    }
}


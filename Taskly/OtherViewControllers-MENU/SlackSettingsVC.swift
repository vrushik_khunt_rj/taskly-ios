//
//  SlackSettingsVC.swift
//  Taskly
//
//  Created by mac on 25/03/22.
//

import UIKit

class SlackSettingsVC: UIViewController {
    @IBOutlet weak var moduleCV: UICollectionView!
    var arrModuleItems = [ModuleItem(name: "Project create", selected: true),
                          ModuleItem(name: "Milestone create", selected: true),
                          ModuleItem(name: "Invoice create", selected: true),
                          ModuleItem(name: "Task create", selected: true),
                          ModuleItem(name: "Milestone status", selected: false),
                          ModuleItem(name: "Invoice status updated", selected: true),
                          ModuleItem(name: "Task move", selected: true),
                          ModuleItem(name: "Task comment", selected: false)]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SlackSettingsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrModuleItems.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ModuleCell", for: indexPath) as! ModuleCell
        cell.configureCell(arrModuleItems[indexPath.item])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3 , height: collectionView.frame.height / 3)
    }
    
}

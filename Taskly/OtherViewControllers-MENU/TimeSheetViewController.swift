//
//  TimeSheetViewController.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit

class TimeSheetViewController: UIViewController {
    @IBOutlet weak var cvTimeSheetWeek: UICollectionView!
    @IBOutlet weak var cvTimeSheetHoriz: UICollectionView!
    @IBOutlet weak var tableTimeSheetList: UITableView!

    var arrTimeSheetList = [TimeSheetListItem(day: "Monday", date: "12 Apr", time: "03:50"),
                            TimeSheetListItem(day: "Tuesday", date: "13 Apr", time: "03:40"),
                            TimeSheetListItem(day: "Wednesday", date: "14 Apr", time: ""),
                            TimeSheetListItem(day: "Thursday", date: "15 Apr", time: "02:50"),
                            TimeSheetListItem(day: "Friday", date: "16 Apr", time: ""),
                            TimeSheetListItem(day: "Saturday", date: "17 Apr", time: "04:50"),
                            TimeSheetListItem(day: "Sunday", date: "18 Apr", time: "03:55")]
    override func viewDidLoad() {
        super.viewDidLoad()
        cvTimeSheetWeek.register(UINib.init(nibName: "TimeSheetListCell", bundle: nil), forCellWithReuseIdentifier: "TimeSheetListCell")
        cvTimeSheetHoriz.register(UINib.init(nibName: "TimeSheetListCell", bundle: nil), forCellWithReuseIdentifier: "TimeSheetListCell")
        tableTimeSheetList.register(UINib.init(nibName: "TimeSheetListTVCell", bundle: nil), forCellReuseIdentifier: "TimeSheetListTVCell")
        tableTimeSheetList.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }


}
extension TimeSheetViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeSheetListTVCell") as! TimeSheetListTVCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension TimeSheetViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.cvTimeSheetWeek{
            return arrTimeSheetList.count
        }
        return arrTimeSheetList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeSheetListCell", for: indexPath) as! TimeSheetListCell
        cell.configureCell(timeItem: arrTimeSheetList[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 4, height: 100)
    }
}

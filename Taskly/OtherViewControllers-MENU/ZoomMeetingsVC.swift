//
//  ZoomMeetingsVC.swift
//  Taskly
//
//  Created by mac on 25/03/22.
//

import UIKit

class ZoomMeetingsVC: UIViewController {
    @IBOutlet weak var segmentMoney: HBSegmentedControl!
    @IBOutlet weak var tableMeetings: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableMeetings.register(UINib.init(nibName: "ZoomMeetingCell", bundle: nil), forCellReuseIdentifier: "ZoomMeetingCell")
        tableMeetings.tableFooterView = UIView()
        
        segmentMoney.items = ["Today", "Week", "Month"]
        segmentMoney.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentMoney.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentMoney.selectedLabelColor = .white
        segmentMoney.backgroundColor = .clear
        segmentMoney.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentMoney.selectedIndex = 0

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ZoomMeetingsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ZoomMeetingCell") as! ZoomMeetingCell
        cell.selectionStyle = .none
        return cell
    }
}

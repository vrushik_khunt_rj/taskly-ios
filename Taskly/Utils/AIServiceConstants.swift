//
//  AIServiceConstants.swift
//
//  Created by Vrushik on 08/02/2021.
//  Copyright © 2021 Vrushik. All rights reserved.
//
import Foundation

//MARK:- BASE URL

//Live URL
let URL_BASE                              = "https://65.1.23.156"


//Test URL
//let URL_BASE                            = "http://192.168.0.200:4932/"
//let URL_Local_Server                    = "http://192.168.0.114:8000/"

let URL_Signup                          = getFullUrl("/wp-json/wp/v2/user/register")

    
//MARK:- FULL URL

func getFullUrl(_ urlEndPoint : String) -> String {
    return URL_BASE + urlEndPoint
}


extension String
{

static let str_please_enable_camera_permission_in_setting = "Please enable camera Permission in Settings > Taskly > enable camera permission"
    static let str_settings = "Settings"
}

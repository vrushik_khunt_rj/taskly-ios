
//
//  AIUtilsManager.swift
//  Swift3CodeStructure
//
//  Created by Vrushik on 25/11/2016.
//  Copyright © 2016 Vrushik. All rights reserved.
//

import Foundation
import UIKit

let CUSTOM_ERROR_DOMAIN = "CUSTOM_ERROR_DOMAIN"
let CUSTOM_ERROR_USER_INFO_KEY = "CUSTOM_ERROR_USER_INFO_KEY"

// MARK: - INTERNET CHECK

func IS_INTERNET_AVAILABLE() -> Bool{
    return AIReachabilityManager.sharedManager.isInternetAvailableForAllNetworks()
}

func SHOW_INTERNET_ALERT(){
    HIDE_CUSTOM_LOADER()
    HIDE_NETWORK_ACTIVITY_INDICATOR()
    
    displayAlertWithTitle(APP_NAME, andMessage: "Please check your internet connection and try again.", buttons: ["Dismiss"], completion: nil)
}

// MARK: - ALERT
func displayAlertWithMessage(_ message:String) -> Void {
    displayAlertWithTitle(APP_NAME, andMessage: message, buttons: ["OK"], completion: nil)
}


func displayAlertWithMessageFromVC(_ vc:UIViewController, message:String, buttons:[String], completion:((_ index:Int) -> Void)!) -> Void {
    
    let alertController = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
    
    for index in 0..<buttons.count	{
        
        alertController.setValue(NSAttributedString(string: APP_NAME, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16),NSAttributedString.Key.foregroundColor : APP_HEADER_TEXT_COLOR]), forKey: "attributedTitle")
        
        alertController.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14),NSAttributedString.Key.foregroundColor : APP_HEADER_TEXT_COLOR]), forKey: "attributedMessage")
        
        
        let action = UIAlertAction(title: buttons[index], style: .default, handler: {
            (alert: UIAlertAction!) in
            if(completion != nil){
                completion(index)
            }
        })
        
        action.setValue(UIColor.black, forKey: "titleTextColor")
        alertController.addAction(action)
    }
    
    vc.present(alertController, animated: true, completion: nil)
}

func displayAlertWithTitle(_ title:String, andMessage message:String, buttons:[String], completion:((_ index:Int) -> Void)!) -> Void {
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    for index in 0..<buttons.count    {
        
      //  let titleFont:UIFont = init(name: FONT_Raleway_Bold, size: 17)
      
        //App was crashing in oFfline mode because the font name Raleway-Bold so i changed it to system font.
        
        alertController.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0),NSAttributedString.Key.foregroundColor : APP_HEADER_TEXT_COLOR]), forKey: "attributedTitle")
        
        alertController.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedMessage")
        
        
        let action = UIAlertAction(title: buttons[index], style: .default, handler: {
            (alert: UIAlertAction!) in
            if(completion != nil){
                completion(index)
            }
        })
        
        action.setValue(UIColor.black, forKey: "titleTextColor")
        alertController.addAction(action)
    }
    appDelegate.window!.rootViewController?.present(alertController, animated: true, completion:nil)
}

func displayAlertWithTitleForAmazon(_ vc: UIViewController, _ message:String, _ amazonUrl: String, completion:(_ index: Int) -> Void?) -> Void{
    let alert = UIAlertController(title: "Error", message: nil, preferredStyle: .alert)
    let textView = UITextView()
    textView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    let controller = UIViewController()

    textView.frame = controller.view.frame
    controller.view.addSubview(textView)
    textView.backgroundColor = .clear

    alert.setValue(controller, forKey: "contentViewController")
    alert.setValue(NSAttributedString(string: "Error", attributes: [NSAttributedString.Key.font : UIFont.init(name: FONT_ProximaNova_Bold, size: 17)!,NSAttributedString.Key.foregroundColor : APP_HEADER_TEXT_COLOR]), forKey: "attributedTitle")
//
//        alertController.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16),NSAttributedString.Key.foregroundColorNSAttributedString.Key.foregroundColor : APP_HEADER_TEXT_COLOR]), forKey: "attributedTitle")
    alert.setValue(NSAttributedString(string: "Error", attributes: [NSAttributedString.Key.font : UIFont.init(name: FONT_ProximaNova_Bold, size: 17)!,NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedTitle")
   
    alert.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont.init(name: FONT_ProximaNova_Semibold, size: 14)!,NSAttributedString.Key.foregroundColor : APP_HEADER_TEXT_COLOR]), forKey: "attributedMessage")


    let height: NSLayoutConstraint = NSLayoutConstraint(item: alert.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 170)
    alert.view.addConstraint(height)
    let strMsg = String(format: "You can manually buy product from")
   
    let mutableAttributedString = NSMutableAttributedString()

    let attributedString = NSMutableAttributedString(string: strMsg)
    let attributedString1 = NSMutableAttributedString(string: " Amazon")
    let url = URL(string: amazonUrl)

    attributedString.setAttributes([.font : UIFont.init(name: FONT_ProximaNova_Regular, size: 14)!], range: NSMakeRange(0, attributedString.length))
    attributedString1.setAttributes([.link: url ?? "",.font : UIFont.init(name: FONT_ProximaNova_Semibold, size: 14)!,.foregroundColor:APP_PRIMARY_ORANGE_COLOR], range: NSMakeRange(0, attributedString1.length))
    mutableAttributedString.append(attributedString)
    mutableAttributedString.append(attributedString1)


    textView.attributedText = mutableAttributedString
    textView.textAlignment = .center
    
    textView.isUserInteractionEnabled = true
    textView.isEditable = false

    // Set how links should appear: blue and underlined
    textView.linkTextAttributes = [.font : UIFont.init(name: FONT_ProximaNova_Semibold, size: 14)!,
    .foregroundColor: APP_PRIMARY_ORANGE_COLOR]

    let okAction = UIAlertAction(title: "OK", style: .default) {
    UIAlertAction in
    }
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
//        UIAlertAction in
//        }
    okAction.setValue(APP_HEADER_TEXT_COLOR, forKey: "titleTextColor")

    alert.addAction(okAction)
//        alert.addAction(cancelAction)

    vc.present(alert, animated: true, completion: nil)

}


func displayAlertWithTitle(_ vc:UIViewController, title:String, andMessage message:String, buttons:[String], completion:((_ index:Int) -> Void)!) -> Void {
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    for index in 0..<buttons.count	{
        
        alertController.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont.init(name: FONT_ProximaNova_Bold, size: 17)!,NSAttributedString.Key.foregroundColor : APP_HEADER_TEXT_COLOR]), forKey: "attributedTitle")
//
//        alertController.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16),NSAttributedString.Key.foregroundColorNSAttributedString.Key.foregroundColor : APP_HEADER_TEXT_COLOR]), forKey: "attributedTitle")
        alertController.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont.init(name: FONT_ProximaNova_Bold, size: 17)!,NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedTitle")
        
        alertController.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont.init(name: FONT_ProximaNova_Semibold, size: 14)!,NSAttributedString.Key.foregroundColor : APP_HEADER_TEXT_COLOR]), forKey: "attributedMessage")
        
        
        let action = UIAlertAction(title: buttons[index], style: .default, handler: {
            (alert: UIAlertAction!) in
            if(completion != nil){
                completion(index)
            }
        })
        
        action.setValue(APP_HEADER_TEXT_COLOR, forKey: "titleTextColor")
        alertController.addAction(action)
    }
    vc.present(alertController, animated: true, completion: nil)
}


func SHOW_NETWORK_ACTIVITY_INDICATOR(){
    UIApplication.shared.isNetworkActivityIndicatorVisible =  true
}

func HIDE_NETWORK_ACTIVITY_INDICATOR(){
    UIApplication.shared.isNetworkActivityIndicatorVisible =  false
}

func isValidEmail(_ stringToCheckForEmail:String) -> Bool {
    let emailRegex = "[A-Z0-9a-z]+([._%+-]{1}[A-Z0-9a-z]+)*@[A-Z0-9a-z]+([.-]{1}[A-Z0-9a-z]+)*(\\.[A-Za-z]{2,4}){0,1}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: stringToCheckForEmail)
    
}

func isValidateCourse(_ strToCheckCourseName: String) -> Bool {
    let courseName = "[A-Z0-9a-z]+"
    return NSPredicate(format: "SELF MATCHES %@", courseName).evaluate(with: strToCheckCourseName)
}
// MARK: - DATE MERGING

func combineDateWithTime(date: NSDate, time: NSDate) -> NSDate? {
    let calendar = NSCalendar.current
    
    let dateComponents = calendar.dateComponents([.year,.month,.day], from: date as Date)
    
    let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: time as Date)
    
    let mergedComponments = NSDateComponents()
    mergedComponments.year = dateComponents.year!
    mergedComponments.month = dateComponents.month!
    mergedComponments.day = dateComponents.day!
    mergedComponments.hour = timeComponents.hour!
    mergedComponments.minute = timeComponents.minute!
    mergedComponments.second = timeComponents.second!
    
    return calendar.date(from: mergedComponments as DateComponents) as NSDate?
}

// MARK:-  DATE STRING TO DATE TYPE CASTING //Formate : "yyyy-MM-dd'T'HH:mm:ssZ"

func appStringTODateConvert(strDate:String)-> String{
    let dateFormatter = DateFormatter()
    let tempLocale = dateFormatter.locale // save locale temporarily
    dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"  // "yyyy-MM-dd'T'HH:mm:ss.SSSZZ

    guard let date = dateFormatter.date(from: strDate) else {
        return appStringTODateConvertAnotherFormate(dateString: strDate)
    }

    dateFormatter.dateFormat = "MM/dd/yyyy"
    dateFormatter.locale = tempLocale // reset the locale
    let dateString = dateFormatter.string(from: date)
    
    return dateString.isEmpty ? "" : dateString
    
}

func appStringTODateConvertAnotherFormate(dateString:String)-> String{
    
    let dateFormatter = DateFormatter()
    let tempLocale = dateFormatter.locale // save locale temporarily
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    guard let date = dateFormatter.date(from: dateString) else {
        return ""
    }
    
    dateFormatter.dateFormat = "MM/dd/yyyy"
    dateFormatter.locale = tempLocale // reset the locale
    let dateString = dateFormatter.string(from: date)
    return dateString.isEmpty ? "" : dateString
    
}

func gateStringToDate(strDate:String) -> Date{
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    
    guard let date = dateFormatter.date(from: strDate) else {

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        guard let date1 = dateFormatter.date(from: strDate) else {
            return Date()
        }
        return date1
    }
    return date
}

func getOffsetFromCurrentDate(strDate:String) -> String {
    var returnValue = ""
    
    let dateCheck = gateStringToDate(strDate: strDate)
    
    let differenceComponent = NSCalendar.current.dateComponents([.second, .minute, .hour, .day, .month, .year], from: dateCheck, to: NSDate() as Date)

    
    if differenceComponent.year! > 0 || differenceComponent.month! > 0 || differenceComponent.day! > 2{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        returnValue = dateFormatter.string(from: dateCheck)
        
    }else if differenceComponent.day! <= 2{
        if differenceComponent.day == 1 {
            returnValue = "1d"
        }else{
            returnValue = String(format: "%dd", differenceComponent.day!)
        }
        
    }else if differenceComponent.hour! > 0{
        if differenceComponent.hour == 1 {
            returnValue = "1h"
        }else{
            returnValue = String(format: "%dh", differenceComponent.hour!)
        }
    }else if differenceComponent.minute! > 0{
        if differenceComponent.minute == 1 {
            returnValue = "1m"
        }else{
            returnValue = String(format: "%dm", differenceComponent.minute!)
        }
    }else if differenceComponent.second! >= 0{
        //returnValue = "Just Now"
            if differenceComponent.second == 1 {
                returnValue = "1s"
            }else{
                returnValue = String(format: "%ds", differenceComponent.second!)
            }
    }
    
    return returnValue
}

extension String {
    
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "[6789][0-9]{6}([0-9]{3})?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}

extension UIButton {
    public func SetCornerRadius(_ Radius : Float) {
        
        self.layer.cornerRadius = CGFloat(Radius)
        self.clipsToBounds = true
        
      }
}

//MARK:- DEVICE CHECK

//Check IsiPhone Device
func IS_IPHONE_DEVICE()->Bool{
    let deviceType = UIDevice.current.userInterfaceIdiom == .phone
    return deviceType
}

//Check IsiPad Device
func IS_IPAD_DEVICE()->Bool{
    let deviceType = UIDevice.current.userInterfaceIdiom == .pad
    return deviceType
}


//iPhone 4 OR 4S
func IS_IPHONE_4_OR_4S()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 480
    var device:Bool = false
    
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)	{
        device = true
    }
    return device
}

//iPhone 5 OR OR 5C OR 4S
func IS_IPHONE_5_OR_5S()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 568
    var device:Bool = false
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)	{
        device = true
    }
    return device
}

//iPhone 6 OR 6S
func IS_IPHONE_6_OR_6S()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 667
    var device:Bool = false
    
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)	{
        device = true
    }
    return device
}

//iPhone 6Plus OR 6SPlus
func IS_IPHONE_6P_OR_6SP()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 736
    var device:Bool = false
    
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)	{
        device = true
    }
    return device
}

//iPhone X and Xs
func IS_IPHONE_X_OR_XS()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 812
    var device:Bool = false
    
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)    {
        device = true
    }
    return device
}

//iPhone XR and Xs Max
func IS_IPHONE_XR_OR_XS_MAX()->Bool{
    let SCREEN_HEIGHT_TO_CHECK_AGAINST:CGFloat = 896
    var device:Bool = false
    
    if(SCREEN_HEIGHT_TO_CHECK_AGAINST == SCREEN_HEIGHT)    {
        device = true
    }
    return device
}

//MARK:- DEVICE ORIENTATION CHECK
func IS_DEVICE_PORTRAIT() -> Bool {
    return UIDevice.current.orientation.isPortrait
}

func IS_DEVICE_LANDSCAPE() -> Bool {
    return UIDevice.current.orientation.isLandscape
}



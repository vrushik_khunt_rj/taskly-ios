//
//  MenuViewController.swift
//  Taskly
//
//  Created by mac on 09/03/22.
//

import UIKit
import SOTabBar
import Alamofire
protocol MenuOptionSelectDelegate: NSObject{
    func menuOptionSelected(_ title: String)
}

class MenuViewController: UIViewController {
    @IBOutlet weak var menuCollection: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    var mDelegate: MenuOptionSelectDelegate?
    var parentVC: UIViewController!
    var selectedItem: String!
    var arrMenuItems = [MenuItem(title: "Dashboard", image: "tab_dash_sel", isselected: true),
                        MenuItem(title: "Zoom Meeting", image: "ic_zoom", isselected: false),
                        MenuItem(title: "Notes", image: "ic_notes", isselected: false),
                        MenuItem(title: "Clients", image: "ic_clients", isselected: false),
                        MenuItem(title: "Calendar", image: "ic_calendar", isselected: false),
                        MenuItem(title: "Projects", image: "ic_project", isselected: false),
                        MenuItem(title: "Settings", image: "tab_setting_sel", isselected: false),
                        MenuItem(title: "Timesheet", image: "ic_timesheet", isselected: false),
                        MenuItem(title: "Subscription", image: "ic_subscription", isselected: false),
                        MenuItem(title: "Plans", image: "ic_plans", isselected: false)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        for item in arrMenuItems{
            if selectedItem == item.title{
                item.isSelected = true
            }else{
                item.isSelected = false
            }
        }
        lblTitle.text = selectedItem != nil ? selectedItem : "Dashboard"
        menuCollection.register(UINib.init(nibName: "MenuCell", bundle: nil), forCellWithReuseIdentifier: "MenuCell")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        let transition:CATransition = CATransition()
//        transition.duration = 0.3
//        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromBottom
//        view.layer.add(transition, forKey: kCATransition)
        self.view.alpha = 1
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
//        let transition:CATransition = CATransition()
//        transition.duration = 0.3
//        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromTop
//        view.layer.add(transition, forKey: kCATransition)
//        self.view.alpha = 0

        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickEditProfile(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
        self.dismiss(animated: false) {
            self.parentVC.navigationController?.pushViewController(vc, animated: true)
        }
    }



}
extension MenuViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 4, height: collectionView.frame.width / 3.6)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMenuItems.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.configureCell(arrMenuItems[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let _ = self.mDelegate{
//            self.mDelegate?.menuOptionSelected(arrMenuItems[indexPath.row].title)
        self.dismiss(animated: false) {
            if let _ = self.parentVC{

                let title = self.arrMenuItems[indexPath.item].title
                var viewContr = UIViewController()
                var isTabVC:Bool = false
                
                if title == "Notes"{
                    viewContr = NotesViewController(nibName: "NotesViewController", bundle: nil)
                }else if title == "Projects"{
                    viewContr = ProjectsViewController(nibName: "ProjectsViewController", bundle: nil)
                }else if title == "Dashboard"{
                    let imageDataDict:[String: Int] = ["tab": 2]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }else if title == "Zoom Meeting"{
                    isTabVC = false
                    viewContr = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ZoomMeetingsVC")
                }else if title == "Calendar"{
                    isTabVC = false
                    viewContr = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CalendarVC")
                }
                else if title == "Tasks"{
                    let imageDataDict:[String: Int] = ["tab": 0]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)


                }else if title == "Invoices"{
                    let imageDataDict:[String: Int] = ["tab": 1]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }else if title == "Users"{
                    let imageDataDict:[String: Int] = ["tab": 3]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }else if title == "Settings"{
                    viewContr = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SettingsVC")

                }else if title == "Clients"{
                    isTabVC = false
                    viewContr = ClientsVC(nibName: "ClientsVC", bundle: nil)
                }else if title == "Timesheet"{
                    isTabVC = false
                    viewContr = iPhoneStoryBoard.instantiateViewController(withIdentifier: "TimeSheetViewController")
                }else if title == "Subscription"{
                    isTabVC = false
                    viewContr = SubscriptionViewController(nibName: "SubscriptionViewController", bundle: nil)
                }
                else if title == "Tracker"{
                    let imageDataDict:[String: Int] = ["tab": 4]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }else if title == "Plans"{
                    isTabVC = false
                    viewContr = PlansViewController(nibName: "PlansViewController", bundle: nil)
                }
                
                if !isTabVC{
                    self.parentVC.navigationController?.pushViewController(viewContr, animated: true)
                }

            }

        }
//        }
    }

}
